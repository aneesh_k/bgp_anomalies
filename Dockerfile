FROM kulkarniane/bgp

MAINTAINER Aneesh Kulkarni

LABEL description="NBGP"

WORKDIR /

ENTRYPOINT ["python", "ip_tcp_refragment.py"]

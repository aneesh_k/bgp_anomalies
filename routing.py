import sys
import time
import yaml
from mrtparse import *

as_peers = dict()
as_ip = dict()
as_nlri = dict()

as_ip_2 = dict()


def print_yaml(timestamp, prefix, as_path_list):
    op = dict(
        hijack_attempt = dict(
            timestamp = timestamp,
            prefix = str(prefix),
            as_path = as_path_list
        )
    )
    print("---")
    print(yaml.dump(op, explicit_start=True, default_flow_style=False))
    return


def check_nlri_prefix(nlri_prefix, nlri_len):
    length = len(nlri_prefix)
    nlri_dict = dict()
    return_prefix = list()
    for val in nlri_prefix:
        nlri_dict[val] = list()

    i = 0
    while i < length:
        prefix = nlri_prefix[i]
        len_nlri = nlri_len[i]
        # if len_nlri not in nlri_dict[prefix]:
        nlri_dict[prefix].append(len_nlri)
        i += 1
    for key, val in nlri_dict.items():
        if len(val) > 1:
            return_prefix.append(key)

    if len(return_prefix) == 0:
        return False
    else:
        return return_prefix


# def create_peers():
#     pass
#
#
# def create_paths():
#     pass
#
#
# def create_trust():
#     pass


def check_loops(as_path):
    counter = dict()
    for as_val in as_path:
        as_val = int(as_val)
        try:
            if counter[as_val] >= 0:
                counter[as_val] += 1
        except KeyError:
            counter[as_val] = 0
    for key, val in counter.items():
        if val > 0:
            return True

    return False


def check_key_list(asno, ip):
    flag = 0
    try:
        if as_peers[asno]:
            flag = 1
    except KeyError:
        as_peers[asno] = list()
        flag = 1

    try:
        if as_nlri[asno]:
            flag = 1
    except KeyError:
        as_nlri[asno] = list()
        flag = 1

    return


def check_as_ip(asno):
    try:
        if as_ip[asno]:
            flag = 1
    except KeyError:
        as_ip[asno] = None
        flag = 1
    return


def check_as_ip_2(asno):
    try:
        if as_ip_2[asno]:
            flag = 1
    except KeyError:
        as_ip_2[asno] = None
        flag = 1
    return


# Collecting values and checking

def main(bgp_as):
    bgp_updates = Reader(bgp_as)
    for update in bgp_updates:
        red_flag = False
        current_nlri_prefix_list = list()
        current_nlri_len_list = list()
        current_nlri_dict = dict()
        bgpU = update.mrt
        if bgpU.err == MRT_ERR_C['MRT Header Error']:
            return
        elif bgpU.type == MRT_T['BGP4MP'] or bgpU.type == MRT_T['BGP4MP_ET']:
            timestamp = bgpU.ts
            peer_as = bgpU.bgp.peer_as
            local_as = bgpU.bgp.local_as
            peer_ip = str(bgpU.bgp.peer_ip)
            local_ip = str(bgpU.bgp.local_ip)
            check_key_list(local_as, local_ip)
            check_key_list(peer_as, peer_ip)

            if bgpU.subtype in [BGP4MP_ST["BGP4MP_MESSAGE"], BGP4MP_ST["BGP4MP_MESSAGE_AS4"]]:
                bgp_msg = bgpU.bgp.msg
                if bgp_msg.type == BGP_MSG_T["UPDATE"]:
                    if bgp_msg.wd_len != 0:
                        check_as_ip(peer_as)
                        check_as_ip(local_as)

                        if as_ip[peer_as] is None:
                            as_ip[peer_as] = peer_ip
                        elif as_ip[peer_as] != local_ip:
                            red_flag = True
                            # red_flag = False
                        if as_ip[local_as] is None:
                            as_ip[local_as] = local_ip
                        elif as_ip[peer_as] != peer_ip:
                            red_flag = True
                            # red_flag = False

                        for withdraw in bgp_msg.withdrawn:
                            prefix = withdraw.prefix
                            withdraw_len = withdraw.plen

                    if bgp_msg.attr_len != 0:
                        for attr in bgp_msg.attr:
                            if attr.type == BGP_ATTR_T['ORIGIN']:
                                origin = int(attr.origin)
                                if origin == 2:
                                    check_as_ip_2(local_as)
                                    if as_ip[peer_as] is None:
                                        as_ip[peer_as] = peer_ip
                            elif attr.type == BGP_ATTR_T['AS_PATH'] or attr.type == BGP_ATTR_T['AS4_PATH']:
                                for path_seg in attr.as_path:
                                    path_len = path_seg['len']
                                    segment_val = path_seg['val'] # ' '.join(path_seg['val']

                                    loops = check_loops(segment_val)
                                    if loops is True:
                                        red_flag = True
                                        # red_flag = False



                    for nlri in bgp_msg.nlri:
                        msg_nlri_prefix = nlri.prefix
                        msg_nlri_len = nlri.plen
                        current_nlri_prefix_list.append(msg_nlri_prefix)
                        current_nlri_len_list.append(msg_nlri_len)

                    # nlri_flag = check_nlri_prefix(current_nlri_prefix_list, current_nlri_len_list)
                    # if nlri_flag is not False:
                    #     for ip_prefix in nlri_flag:
                    #         print_yaml(timestamp, ip_prefix, segment_val)

        if red_flag is True:
            for print_nlri in current_nlri_prefix_list:
                print_yaml(timestamp, print_nlri, segment_val)


if __name__ == '__main__':
    try:
        if len(sys.argv) < 2:
            sys.exit(0)
        arguments = sys.argv[1:]
        for argument in arguments:
            main(argument)
    # except Exception as e:
    #     sys.exit(0)
    except KeyboardInterrupt:
        sys.exit(0)
